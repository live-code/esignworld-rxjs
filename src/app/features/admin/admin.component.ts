import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'ew-admin',
  template: `
      <div *ngIf="error">credenziali errate</div>
      {{data | json}}
  `,
})
export class AdminComponent implements OnInit {
  data: any[];
  error: boolean;

  constructor(private http: HttpClient) {
    http.get<any[]>('http://localhost:3000/users')
      .subscribe(
        res => this.data = res,
        err => {
          this.error = true;
        }
      );

  }

  ngOnInit(): void {
  }

}
