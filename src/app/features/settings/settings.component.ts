import { Component, OnDestroy, OnInit } from '@angular/core';
import { Config, ConfigService } from '../../core/services/config.service';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ew-settings',
  template: `
    
    <pre>{{configService.data$ | async | json}}</pre>
    <button (click)="configService.setValue({ color: 'pink'})">pink</button>
    <button (click)="configService.setValue({ color: 'blue'})">blue</button>
  `,
})
export class SettingsComponent implements OnDestroy {
  data: Config;
  destroy$: Subject<void> = new Subject<void>();

  constructor(public configService: ConfigService) {
    configService.data$
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.data = res;
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

}
