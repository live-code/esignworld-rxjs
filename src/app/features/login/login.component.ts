import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';
import { filter, first } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ew-login',
  template: `
    <p>
      login works!
      
    </p>
    <button (click)="loginHandler()">Login</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit, OnDestroy {
  sub: Subscription;

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.sub = this.authService.isLogged$
      .pipe(
        filter(isLogged => isLogged)
      )
      .subscribe(() => {
        this.router.navigateByUrl('home')
      });
  }

  loginHandler(): void {
    this.authService.login('a', 'b')
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
