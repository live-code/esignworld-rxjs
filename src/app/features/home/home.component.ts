import { Component, ComponentFactoryResolver, OnInit, Renderer2, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../../shared/card.component';
import { TabbarComponent } from '../../shared/tabbar.component';

@Component({
  selector: 'ew-home',
  template: `
    <div>1</div>
    <ng-container *ngFor="let config of json">
      <div  
        ewLoader [config]="config"
        (loaderEvent)="doSomething($event)"
      >
        <div id="content">xxx</div>
      </div>
    </ng-container>
    <div>2</div>

    
  `,
})
export class HomeComponent implements OnInit {
  json = [
    { type: 'card', data: { title: 'miao', icon: 'fa fa-icons' }, events: ['iconClick'] },
    { type: 'card', data: { title: 'baao'}, events: [] },
    { type: 'tab', data: { items: [1, 2, 3]}, events: ['tabClick'] },
  ];

  ngOnInit(): void {
  }


  doSomething(params): void {
    console.log(' do something', params)
  }
}
