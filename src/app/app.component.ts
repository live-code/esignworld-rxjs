import { Component } from '@angular/core';

@Component({
  selector: 'ew-root',
  template: `
    <ew-navbar></ew-navbar>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'esignworld-rxjs';
}
