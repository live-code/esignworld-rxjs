import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DynamicComponent } from './dynamic-component';

@Component({
  selector: 'ew-tabbar',
  template: `
    {{title}}
    <p (click)="tabClick.emit(123)">
      tabbar works! {{items}}
    </p>
  `,
  styles: [
  ]
})
export class TabbarComponent implements DynamicComponent {
  @Input() title: string;
  @Output() pluto: EventEmitter<any> = new EventEmitter<any>();

  @Input() items: any[];
  @Output() tabClick: EventEmitter<number> = new EventEmitter();


  doSomething(): void {

  }
}
