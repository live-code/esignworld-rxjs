import { CardComponent } from './card.component';
import { TabbarComponent } from './tabbar.component';
import { Type } from '@angular/core';
import { DynamicComponent } from './dynamic-component';

export const COMPONENTS: {[key: string]: Type<DynamicComponent>} = {
  card: CardComponent,
  tab: TabbarComponent
};
