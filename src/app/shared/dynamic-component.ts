import { EventEmitter } from '@angular/core';

export interface DynamicComponent {
  title: string;
  pluto: EventEmitter<any>;

  doSomething: (a: number) => void;
}
