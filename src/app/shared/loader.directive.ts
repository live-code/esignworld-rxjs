import {
  ComponentFactory,
  ComponentFactoryResolver, ComponentRef,
  Directive, ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewContainerRef
} from '@angular/core';
import { CardComponent } from './card.component';
import { TabbarComponent } from './tabbar.component';
import { DynamicComponent } from './dynamic-component';
import { COMPONENTS } from './dynamic-map';

@Directive({
  selector: '[ewLoader]'
})
export class LoaderDirective implements OnInit {
  @Input() config: any;
  @Output() loaderEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private view: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
    private element: ElementRef
  ) {
  }

  ngOnInit(): void {
    const content = this.element.nativeElement.querySelector('#content');
    content.innerHTML = 'ypp!';
    const factory = this.resolver.resolveComponentFactory<DynamicComponent>(COMPONENTS[this.config.type]  );
    const compo: ComponentRef<DynamicComponent> = this.view.createComponent(factory);

    for (const key in this.config.data) {
      if (key) {
        compo.instance[key] = this.config.data[key];
      }
    }

    for (const event of this.config.events) {
      console.log(event)

      if (compo.instance[event]) {
        compo.instance[event].subscribe(params => {
          this.loaderEvent.emit({ eventType: event, params });
        })
      }
    }
  }

}
