import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DynamicComponent } from './dynamic-component';

@Component({
  selector: 'ew-card',
  template: `
    <p>
      card works! {{title}}
    </p>
    
    <div *ngIf="icon" (click)="iconClick.emit()">sono un'icona</div>
  `,
  styles: [
  ]
})
export class CardComponent implements DynamicComponent, OnInit {
  @Input() title: string;
  @Input() icon: string;
  @Output() iconClick = new EventEmitter()
  pluto: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
  }

  doSomething(a: number): void {
  }


}
