import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { filter, map, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivateWithServerSideCheck(): Observable<boolean> {
    return this.authService.token$
      .pipe(
        switchMap(token => this.authService.validateToken(token)),
        tap(res => {
          if (res.response !== 'ok') {
            this.authService.logout();
            this.router.navigateByUrl('login');
          }
        }),
        map(res => res.response === 'ok')
      );
  }

  canActivate(): Observable<boolean> {
    return this.authService.isLogged$
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.router.navigateByUrl('login')
          }
        })
      );
  }

}
