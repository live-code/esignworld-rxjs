import { Directive, HostBinding, Input, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';
import { distinctUntilChanged, filter, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Template } from '@angular/compiler/src/render3/r3_ast';

@Directive({
  selector: '[ewIfLogged]'
})
export class IfLoggedDirective implements OnDestroy {
  @Input() ewIfLogged: string;
  destroy$: Subject<any> = new Subject();

  constructor(
    private authService: AuthService,
    private view: ViewContainerRef,
    private template: TemplateRef<any>
  ) {

    authService.isLogged$
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.destroy$),
        tap(() => view.clear()),
        withLatestFrom(this.authService.role$),
        filter(([isLogged, role]) => {
          return (isLogged && !this.ewIfLogged) ||
                 (isLogged && role === this.ewIfLogged);
        })
      )
      .subscribe(() => {
        view.createEmbeddedView(template);
      })
    /*authService.isLogged$
      .subscribe()*/
  }

  ngOnInit() {
    console.log(this.ewIfLogged)
  }
  ngOnDestroy(): void {
    this.destroy$.next()
  }

}
