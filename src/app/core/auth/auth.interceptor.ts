import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { iif, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { catchError, first, map, mergeMap, tap, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$
      .pipe(
        first(),
        /*
        mergeMap(token => {
          let clonedReq = request;
          if (token) {
            clonedReq = request.clone({ setHeaders: { Authorization: token} });
          }
          return next.handle(clonedReq);
        })
        */
       /*
       // FUNZIONA SOLO CON FIRST, VERIFICARE
        mergeMap(token => {
          return this.authService.isLogged$
            .pipe(
              first(),
              map(isLogged => ({ isLogged, token}))
            );
        }),*/
        withLatestFrom(this.authService.isLogged$),
        mergeMap(([token, isLogged]) => iif(
          () => isLogged && !request.url.includes('login'),
          next.handle(request.clone({ setHeaders: { Authorization: token} })),
          next.handle(request)
        )),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 404:
                // ...
                break;

              case 401:
              default:
                this.authService.logout();
                this.router.navigateByUrl('login');
                break;
            }
          }
          return throwError(err);
        })
      );
  }
}
