import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Auth } from './auth';
import { BehaviorSubject, Observable } from 'rxjs';
import { first, map, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);

  constructor(private http: HttpClient) { }

  login(user: string, pass: string): Observable<Auth> {
    const req$ = this.http.get<Auth>('http://localhost:3000/login')
      .pipe(share())
    req$.subscribe(res => this.auth$.next(res));
    return req$;
  }

  logout(): void {
    this.auth$.next(null);
  }

  validateToken(token: string): Observable<{ response: string }> {
    return this.http.get<{ response: string }>('http://localhost:3000/validateToken?tk=' + token)
  }

  get isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(val => !!val)
      );
  }

  get token$(): Observable<string> {
    return this.auth$
      .pipe(
        map(val => val ? val.token : null)
      );
  }

  get displayName$(): Observable<string> {
    return this.auth$
      .pipe(
        map(val => val?.displayName)
      );
  }

  get role$(): Observable<string> {
    return this.auth$
      .pipe(
        map(val => val?.role)
      );
  }

}
