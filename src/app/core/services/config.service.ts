import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { first, map } from 'rxjs/operators';

export interface Config {
  color: string;
  fontSize: number;
  language: 'it' | 'en';
}

const INITIAL_STATE: Config = {
  color: '#222',
  language: 'en',
  fontSize: 20
};

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private myData$: BehaviorSubject<Config> = new BehaviorSubject<Config>(INITIAL_STATE);
  data$ = this.myData$.asObservable();

  setValue(newConfig: Partial<Config>): void {
    this.data$
      .pipe(
        first(),
        map(data => ({ ...data, ...newConfig }))
      )
      .subscribe(config => {
        this.myData$.next(config);
      });
  }
}
