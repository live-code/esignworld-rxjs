import { Component, OnDestroy, OnInit } from '@angular/core';
import { Config, ConfigService } from '../services/config.service';
import { config, Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'ew-navbar',
  template: `
    
    <div 
      style="padding: 20px; color: white" 
      [style.backgroundColor]="(configService.data$ | async).color"
    >
      <button [routerLink]="'home'">home</button>
        <button routerLink="admin" *ewIfLogged="'admin'">admin</button>
        <button routerLink="settings" *ewIfLogged>settings</button>
      <button routerLink="login">login</button>
      <button (click)="authService.logout()">logout</button>
      <div>
        {{authService.displayName$ | async}}
      </div>
    </div>
    <hr>
    
  `,
})
export class NavbarComponent {
  prop = false;

  constructor(
    public configService: ConfigService,
    public authService: AuthService
  ) {

  }
}
